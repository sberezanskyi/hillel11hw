//2. Fix the method with try-catch to get the correct result of division:

public class Task2 {

    public static void main(String[] args) {
        try {
             Task2.divideByZero();
        }
        catch (Exception e) {
            System.err.println("ERROR !!! " + e);
        }
    }

    public static int divideByZero() {
        int a = 5;
        int b = 0;
        return a / b;
    }
}