//5. Throw a NullPointerException from the method iCanThrowException();
//   Handle it in the main method.

public class Task5 {

    public static void iCanThrowException(){
        throw new NullPointerException("exception");
    }

    public static void main(String[] args) {
        try {
            Task5.iCanThrowException();
        }catch (Exception e) {
            System.err.println("ERROR !!! " + e);
        }
    }

}
