//1. Create method to check: if number < 0 print “Number < 0”, if number > 0 print “Number > 0”
// if number = 0 throw your own exception

public class Task1 {

    public static void numberCheck(int number) throws Exception {
        if (number > 0 | number < 0) {
            System.out.println(number > 0 ? "Number > 0" : "Number < 0");
        } else throw new Exception("Number can't was zero");
    }

    public static void main(String[] args) {
        try {
            Task1.numberCheck(0);
//            Task1.numberCheck(7);
        } catch (Exception e) {
            System.err.println("ERROR !!! " + e);
        }
    }

}